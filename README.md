## Зависимости
- NodeJS 8.10
- MongoDB 3.6.3

**Установка NodeJS + npm**

- install nvm: https://github.com/creationix/nvm

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash

source ~/.bashrc

- check

command -v nvm

- it should output 'nvm'

- install NodeJS v8.10 and npm

nvm install 8.10

-  set v8.10 as default version

nvm alias default 8.10

**Установка MongoDB**

Документация: 
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

**Установка pm2**

npm install pm2 -g

**Установка gulp**

npm install gulp -g

**Клонирование репозитория**

cd ./projects_folder

git clone git@gitlab.com:centralprojects/landing.git

**Установка пакетов**

cd ./projects_folder

npm install

**Заполнение тестовыми данными**

cd ./project_folder

gulp build

node ./dist/test_data/insert_test_data.js

**Запуск**

cd ./project_folder

gulp serve

# API

**Signin**

- mthod: POST
- url: '/api/signin'
- params: email, password
- return: user object vs bearer in cookiend and headers

**Signup**

- method: POST
- url: '/api/signin'
- params: name, surname, email, password
- return: user object vs bearer in cookiend and headers

**Get all posts**

- method: GET
- url: '/api/posts'
- params: limit, offset
- return: posts array and bearer || []
- need auth (Bearer)

**Create post**

- method: POST
- url: '/api/posts'
- params: title, text
- return: created post object
- need auth (Bearer)

**Update post**

- method: PUT
- url: '/api/posts/:id'
- params: id, title, text
- return: updated post object
- need auth (Bearer)

**Delete post**

- method: DELETE
- url: '/api/posts'
- params: id
- return: deleted post object
- need auth (Bearer)

**View post**

- method: GET
- url: '/api/posts/:id'
- params: id
- return: post object
- need auth (Bearer)