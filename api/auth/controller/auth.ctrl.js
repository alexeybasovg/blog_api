'use strict';

import dao from '../../../components/dao';

const signUp = async (req, res) => {
    const body = req.body;
    
    const user = new dao.models.User({
        name: body.name || null,
        surname: body.surname || null,
        email: body.email || null,
        password_hash: body.password ? dao.models.User().generatePasswordHash(body.password) : null,
    });
    const bearer = new dao.models.Token();
  
    try{
        const role = await dao.models.Role.findOne({name: 'user'});
        if(!role){
            return res.status(500).send({
                errors: {
                    role: {
                        message: 'Role "user" does not exist'
                    }
                }
            });
        }

        var err = user.validateSync();

        if(err){
            throw err;
        }else{
            await bearer.generateToken();
            await bearer.save()
    
            user.token = bearer.id;
            user.role = role.id;
    
            await user.save();
        }
    }catch(err){
        return res.status(500).send(err);
    }

    res.cookie('Bearer', bearer.token);
    res.header('Authorization', bearer.token);
    
    res.status(200).send({
        name: user.name,
        surname: user.surname,
        email: user.email
    });
};

const signIn = async (req, res) => {
    const body = req.body;
    const user = await dao.models.User.findOne({email: body.email});
    
    if(user && user.validatePassword(body.password)){
        try{
            const bearer = await dao.models.Token.findOne({_id: user.token});
            
            await bearer.generateToken();
            bearer.created = Date.now();
            await bearer.save();

            res.cookie('Bearer', bearer.token);
            res.header('Authorization', bearer.token);

            return res.status(200).send({
                name: user.name,
                surname: user.surname,
                email: user.email
            });
        }catch(err){
            return res.status(500).send(err);
        }
    }
    
    res.status(403).send({
        errors: {
            email: {
                message: 'Email or password are wrong'
            }
        }
    });
};

module.exports = {
    signUp,
    signIn
};

