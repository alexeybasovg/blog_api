'use strict';

import controller from './controller/auth.ctrl';

module.exports = router => {
    router.post('/signin', controller.signIn);
    router.post('/signup', controller.signUp);
};