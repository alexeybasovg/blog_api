'use strict';

import express from 'express';
const router = express.Router();

require('./auth')(router);
require('./post')(router);

module.exports = router;
