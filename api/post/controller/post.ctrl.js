'use strict';

import dao from '../../../components/dao';
import rbac from '../../../components/rbac';

const getAllPosts = async (req, res) => {
    const limit = req.params.limit || 2;
    const offset = req.params.offset || 1;

    try{
        var posts = await dao.models.Post.find({})
            .skip(limit * offset - limit)
            .limit(limit*1);

        if(!posts.length){
            return res.status(404).send();
        }  

        res.cookie('Bearer', rbac.bearer.token);
        res.header('Authorization', rbac.bearer.token);  
        res.status(200).send(posts);
    }catch(err){
        return res.status(500).send(err);
    }
};

const getSinglePost = async (req, res) => {
    res.cookie('Bearer', rbac.bearer.token);
    res.header('Authorization', rbac.bearer.token); 

    try{
        const id = await dao.mongoose.Types.ObjectId.isValid(req.params.id) ? req.params.id : null;
        const post = await dao.models.Post.findOne({_id: id});

        if(post){
            return res.status(200).send(post);
        }

        res.status(404).send();
    }catch(err){
        res.status(500).send(err);
    }
};

const createPost = async (req, res) => {
    const body = req.body;
    const post = new dao.models.Post({
        title: body.title || null,
        text: body.text || null,
        author: rbac.user.id,
    });

    res.cookie('Bearer', rbac.bearer.token);
    res.header('Authorization', rbac.bearer.token); 

    try{
        var error = post.validateSync();
        if(error) throw error;
        
        await post.save(); 
        res.status(200).send(post);

    }catch(err){
        res.status(500).send(err);
    }
};

const updatePost = async (req, res) => {
    const body = req.body;
    const id = dao.mongoose.Types.ObjectId.isValid(req.params.id) ? req.params.id : null;

    res.cookie('Bearer', rbac.bearer.token);
    res.header('Authorization', rbac.bearer.token); 

    try{
        var post = await dao.models.Post.findOne({_id: id});

        if(post){
            post.title = body.title || null;
            post.text = body.text || null;
            post.updated = Date.now();
    
            await post.save();
            return res.status(200).send(post);
        }

        res.status(404).send();
    }catch(err){
        res.status(500).send(err);
    }
};

const deletePost = async (req, res) => {
    const body = req.body;
    const id = dao.mongoose.Types.ObjectId.isValid(req.params.id) ? req.params.id : null;

    res.cookie('Bearer', rbac.bearer.token);
    res.header('Authorization', rbac.bearer.token); 

    try{
        const post = await dao.models.Post.findOne({_id: id});
        if(post){
            await dao.models.Post.remove({_id: id});
            return res.status(200).send(post);
        }
        
        res.status(400).send();
    }catch(err){
        res.status(500).send(err);
    }
};

module.exports = {
    getAllPosts,
    getSinglePost,
    createPost,
    updatePost,
    deletePost
};

