'use strict';

import controller from './controller/post.ctrl';
import rbac from '../../components/rbac';

module.exports = router => {
    router.get('/posts/:id', rbac.can('view_post'), controller.getSinglePost);
    router.get('/posts/:limit/:offset', rbac.can('view_post'), controller.getAllPosts);
    router.post('/posts', rbac.can('create_post'), controller.createPost);
    router.put('/posts/:id', rbac.can('update_post'), controller.updatePost);
    router.delete('/posts/:id', rbac.can('delete_post'), controller.deletePost);
};