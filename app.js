'use strict';

import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import methodOverride from 'method-override';
import config from './config';

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cookieParser({ 
    maxAge: 30*60*1000,
    httpOnly: true
}));

require('./components/dao');

app.use('/api', require('./api'));

app.use((req, res, next) => {
    res.status(404).send();
});

app.listen(config.server.PORT, () => {
    console.log('Server running at port: ', config.server.PORT);
});