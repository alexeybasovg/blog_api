'use strict';

import mongoose from 'mongoose';
import config from '../config';

const connString = config.db.MONGO_CONNECTION_STRING;
const dao = {mongoose, models:{}};

dao.db = mongoose.createConnection(connString, {});

dao.db.on('open', () => {
    console.log(`MongoDB (${connString.replace('mongodb://', '')}): connection established.`);
});

dao.db.on('error', err => {
    console.log(`MongoDB (${connString.replace('mongodb://', '')}) error: `, err);
    process.exit(0);
});

['permission', 'post', 'role', 'user', 'token'].forEach((modelName) => {
    require(`../models/${modelName}.model.js`)(dao);
});

module.exports = dao;
  
  