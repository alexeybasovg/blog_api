'use strict';

import dao from '../components/dao';

const rbac = {user: {}, bearer: {}, permissions: [], isAuth: false};

rbac.clear = () => {
    rbac.bearer = {};
    rbac.isAuth = false;
    rbac.user = {};
    rbac.permissions = [];
};

rbac.getBearer = async (req, res) => {
    try{
        const token = req.header('authorization') || req.cookies.bearer || null;
        rbac.bearer = await dao.models.Token.findOne({token: token}) || null;
    }catch(err){
        return res.status(500).send(err);
    }
};

rbac.getUser = async (req, res) => {
    try{
        await rbac.getBearer(req, res);

        if(rbac.bearer && rbac.bearer.token){
            if(rbac.bearer.hasExpired()){
                await rbac.bearer.generateToken();
                rbac.bearer.created = Date.now();
                await rbac.bearer.save();
            }
    
            rbac.user = await dao.models.User.findOne({token: rbac.bearer.id});
            rbac.isAuth = true;
        }else{
            rbac.clear();
        }
    }catch(err){
        return res.status(500).send(err)
    }
};

rbac.getPermissions = async () => {
    try{
        const role = await dao.models.Role.findOne({_id: rbac.user.role || null});

        if(role){
            rbac.permissions = await Promise.all(role.permissions.map(async function(permission){
                const perm = await dao.models.Permission.findOne({_id: permission});
                return perm.name;
            }));
        }
    }catch(err){
        return res.status(500).send(err)
    }
};

rbac.can = (permission) => {
    return async (req, res, next) => {
        try{
            await rbac.getUser(req, res);
            await rbac.getPermissions();

            if(!rbac.isAuth || rbac.permissions.length === 0 || rbac.permissions.indexOf(permission) === -1){
                res.clearCookie("Bearer");
                res.removeHeader('authorization');
    
                return res.status(403).send({
                    errors: {
                        access: {
                            message: 'Access denied!'
                        }
                    }
                });
            }else{
                next(); 
            }
        }catch(error){
            return res.status(500).send(error);
        }
    }
};

module.exports = rbac;

