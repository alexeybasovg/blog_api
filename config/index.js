'use strict';

module.exports = {
    'server': require('./server'),
    'db': require('./db')
};