'use strict';

const gulp = require('gulp');
const babel = require('gulp-babel');
const sequence = require('gulp-sequence');
const changed = require('gulp-changed');
const del = require('del');
const nodemon = require('gulp-nodemon');

const assemblyPath = './dist';
const paths = {
    api: {
        src: './api/**/*.js',
        dest: assemblyPath + '/api'
    },
    config: {
        src: './config/**/*.js',
        dest: assemblyPath + '/config'
    },
    server: {
        src: ['./app.js', './routes.js'],
        dest: assemblyPath
    },
    models: {
        src: './models/**/*.js',
        dest: assemblyPath + '/models'
    },
    components: {
        src: './components/**/*.js',
        dest: assemblyPath + '/components'
    },
    test_data: {
        src: './test_data/**/*.js',
        dest: assemblyPath + '/test_data'
    }
};

gulp.task('clean', () => {
    return del([assemblyPath]);
});

gulp.task('api', () => {
    return gulp.src(paths.api.src)
        .pipe(changed(paths.api.dest))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(paths.api.dest));
});

gulp.task('config', () => {
    return gulp.src(paths.config.src)
        .pipe(changed(paths.config.dest))
        .pipe(gulp.dest(paths.config.dest));
});

gulp.task('server', () => {
    return gulp.src(paths.server.src)
        .pipe(changed(paths.server.dest))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(paths.server.dest));
});

gulp.task('models', () => {
    return gulp.src(paths.models.src)
        .pipe(changed(paths.models.dest))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(paths.models.dest));
});

gulp.task('components', () => {
    return gulp.src(paths.components.src)
        .pipe(changed(paths.components.dest))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(paths.components.dest));
});

gulp.task('test_data', () => {
    return gulp.src(paths.test_data.src)
        .pipe(changed(paths.test_data.dest))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(paths.test_data.dest));
});

gulp.task('build', sequence('clean', ['api', 'config', 'server', 'models', 'components', 'test_data']));

gulp.task('serve', ['build'], () => {
    return nodemon({
        script: assemblyPath + '/app.js',
        ext: 'js',
        tasks: ['api', 'config', 'server', 'models', 'components', 'test_data'],
        ignore: [assemblyPath, './node_modules']
    });
});

