'use strict';

const modelName = 'Permission';

module.exports = (dao) => {
    const PermissionSchema = new dao.mongoose.Schema({
        name: {
            type: String,
            required: 'Name cannot be blank'
        }
    });

    dao.models[modelName] = dao.db.model(modelName, PermissionSchema);
};