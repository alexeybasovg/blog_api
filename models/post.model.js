'use strict';

const modelName = 'Post';

module.exports = (dao) => {
    const PostSchema = new dao.mongoose.Schema({
        title: {
            type: String,
            required: 'Title cannot be blank'
        },
        text: {
            type: String,
            required: 'Text cannot be blank'
        },
        author: {
            type: dao.mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        created: {
            type: Date,
            default: Date.now()
        },
        updated: {
            type: Date,
            default: Date.now()
        }
    });

    dao.models[modelName] = dao.db.model(modelName, PostSchema);
};