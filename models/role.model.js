'use strict';
const modelName = 'Role';

module.exports = (dao) => {
    var RoleSchema = new dao.mongoose.Schema({
        name: {
            type: String,
            required: 'Name cannot be blank'
        },
        permissions: [
            {
                type: dao.mongoose.Schema.Types.ObjectId,
                ref: 'Permission'
            }
        ]
    });

    RoleSchema.methods.getByName = function(str, cb){
        return this.model(modelName).findOne({name: str}, cb);
    };

    dao.models[modelName] = dao.db.model(modelName, RoleSchema);
};