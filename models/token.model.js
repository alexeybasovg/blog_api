'use strict';

import crypto from 'crypto';

const modelName = 'Token';

module.exports = (dao) => {
    const TokenSchema = new dao.mongoose.Schema({
        token: {
            type: String,
            required: true
        },
        created: {
            type: Date,
            default: Date.now
        }
    });

    TokenSchema.methods.hasExpired = function(){
        var now = new Date();
        return (now - this.created) > 30*60*1000;
    };

    TokenSchema.methods.generateToken = async function(){
        this.token = await crypto.randomBytes(48).toString('hex');
    };

    dao.models[modelName] = dao.db.model(modelName, TokenSchema);
};