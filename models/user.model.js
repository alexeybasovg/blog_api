'use strict';

import bcrypt from 'bcrypt';

const modelName = 'User';

module.exports = (dao) => {
    var UserSchema = new dao.mongoose.Schema({
        name: {
            type: String,
            required: 'Name canot be blank',
            min: [2, 'Minimal name length is 2 symbols'],
            max: [25, 'Minimal name length is 25 symbols']
        },
        surname: {
            type: String,
            required: 'Surname cannot be blank',
            min: [2, 'Minimal surname length is 2 symbols'],
            max: [25, 'Minimal surname length is 25 symbols']
        },
        email: {
            type: String,
            required: 'Email cannot be blank',
            unique: 'This email is alredy exist',
            validate: {
                validator: function(v) {
                  return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v);
                },
                message: '{VALUE} is not a valid email'
              },
        },
        password_hash: {
            type: String,
            required: 'Password cannot be blank'
        },
        token: {
            type: dao.mongoose.Schema.Types.ObjectId,
            ref: 'Token'
        },
        role: {
            type: dao.mongoose.Schema.Types.ObjectId,
            ref: 'Role'
        }
    });

    UserSchema.plugin(require('mongoose-beautiful-unique-validation'));

    UserSchema.methods.generatePasswordHash = function(password){
        return bcrypt.hashSync(password, 10);
    };

    UserSchema.methods.validatePassword = function(password){
        return bcrypt.compareSync(password, this.password_hash);
    };

    dao.models[modelName] = dao.db.model(modelName, UserSchema);
};