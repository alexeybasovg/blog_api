'use strict';

import dao from '../components/dao';

async function addRBAC(){
    //Add permissions
    const view_post = new dao.models.Permission({name: 'view_post'});
    await view_post.save();

    const create_post = new dao.models.Permission({name: 'create_post'});
    await create_post.save();

    const update_post = new dao.models.Permission({name: 'update_post'});
    await update_post.save();

    const delete_post = new dao.models.Permission({name: 'delete_post'});
    await delete_post.save();

    //Add roles
    const adminRole = new dao.models.Role({
        name: 'admin',
        permissions: [
            view_post._id,
            create_post._id,
            update_post._id,
            delete_post._id
        ]
    });
    await adminRole.save();

    const moderatorRole = new dao.models.Role({
        name: 'moderator',
        permissions: [
            view_post._id,
            update_post._id
        ]
    });
    await moderatorRole.save();

    const userRole = new dao.models.Role({
        name: 'user',
        permissions: [
            view_post._id
        ]
    });
    await userRole.save();

    console.log('Roles and permissions added.');
}

async function addUsers(){
    //Add test admin user
    const adminToken = new dao.models.Token();
    await adminToken.generateToken();
    await adminToken.save();

    const adminRole = await dao.models.Role().getByName('admin');
    const adminUser = new dao.models.User({
        name: 'Admin',
        surname: 'Test',
        email: 'admin@admin.com',
        password_hash: dao.models.User().generatePasswordHash('1234567'),
        role: adminRole._id,
        token: adminToken._id
    });
    await adminUser.save();

    //Add test moderator user
    const moderatorToken = new dao.models.Token();
    await moderatorToken.generateToken();
    await moderatorToken.save();

    const moderatorRole = await dao.models.Role().getByName('moderator');
    const moderatorUser = new dao.models.User({
        name: 'Moderator',
        surname: 'Test',
        email: 'moderator@moderator.com',
        password_hash: dao.models.User().generatePasswordHash('1234567'),
        role: moderatorRole._id,
        token: moderatorToken._id
    });
    await moderatorUser.save();

    //Add test user
    const userToken = new dao.models.Token();
    await userToken.generateToken();
    await userToken.save();

    const userRole = await dao.models.Role().getByName('user');
    const user = new dao.models.User({
        name: 'User',
        surname: 'Test',
        email: 'user@user.com',
        password_hash: dao.models.User().generatePasswordHash('1234567'),
        role: userRole._id,
        token: userToken._id
    });
    await user.save();
}

async function init(){
    dao.db.dropDatabase(err => {
        if(err){
            console.log('Drop db error: ', err);
            process.exit(0);
        }
    });

    await addRBAC();
    await addUsers();

    process.exit(0);
}

init();